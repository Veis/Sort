# **Sort**

Jest to jeden z projektów których postanowiłem się podjąć podczas wiosennej przerwy świątecznej.
Strona jest złożona z dwóch plików JS i jednego HTML.

## **Wymagania**
- git

# Instalacja git

**ten krok nie jest wymagany, można także kliknąć przycisk pobierania na stronie repozytorium**

Aby zainstalować git wykonaj poniższe kroki:

Przejdź na stronę https://git-scm.com/downloads/

Pobierz i zainstaluj odpowiednią wersję dla swojego systemu operacyjnego.

Uruchom terminal (wiersz poleceń).

Sprawdź, czy node.js i npm zostały poprawnie zainstalowane, wykonując polecenie:

- git -v

# Uruchomienie strony internetowej
Sklonuj repozytorium lub pobierz na stronie:

```git clone https://codeberg.org/Veis/Sort.git```

Przejdź do folderu z projektem:

```cd Sort```

Uruchom stronę:

Windows: ```start index.html```

MacOS: ```open index.html```

Ten projekt został stworzony przez **Veis**.

# Licencja

Ten projekt działa na licencji MIT, co znaczy że każdy, kto otrzymał kopię projektu, może go używać, kopiować, modyfikować, publikować, dystrybuować, sprzedawać i/lub sublicencjonować. Wymagane jest jedynie, aby każda kopia lub znacząca część projektu zawierała powyższe oświadczenie o prawach autorskich i warunki licencji. Licencja MIT gwarantuje również, że autorzy projektu nie ponoszą odpowiedzialności za jakiekolwiek roszczenia, szkody lub inne zobowiązania wynikające z użytkowania lub niemożności użytkowania projektu.